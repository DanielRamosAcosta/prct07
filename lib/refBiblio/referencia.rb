require 'date'

module RefBiblio
	class Referencia
		attr_accessor :autor, :titulo, :serie, :editorial, :edicion, :publicacion, :isbn
		def initialize(autor, titulo, editorial, edicion, publicacion, isbn, serie=nil)
			raise ArgumentError, "El autor no es un array" unless autor.is_a?(Array)
			autor.each do |a|
				raise ArgumentError, "Uno de los autores no es un string" unless a.is_a?(String)
			end
			raise ArgumentError, "El titulo no es un string" unless titulo.is_a?(String)
			raise ArgumentError, "La serie no es nulo o un string" unless serie.nil? || serie.is_a?(String)
			raise ArgumentError, "La editorial no es un string" unless titulo.is_a?(String)
			raise ArgumentError, "La edicion no es un numero entero" unless edicion.is_a?(Integer)
			raise ArgumentError, "La edicion no es un numero no valido" unless edicion > 0
			raise ArgumentError, "La fecha no es de tipo Date" unless publicacion.is_a?(Date)
			raise ArgumentError, "Los ISBN no son un array" unless isbn.is_a?(Array)
			isbn.each do |i|
				raise ArgumentError, "Uno de los ISBN no es un string" unless i.is_a?(String)
			end

			@autor = autor
			@titulo = titulo
			@serie = serie
			@editorial = editorial
			@edicion = edicion
			@publicacion = publicacion
			@isbn = isbn
		end
		def to_s
			final = ""
			autor.each do |a|
				final << a
				final << ', ' unless a == autor.last
			end
			final << ".\n"
			final << titulo << "\n"
			final << serie << "\n" unless serie == nil
			final << editorial << "; " << edicion.to_s << " edition "
			final << "(" << Date::MONTHNAMES[publicacion.month] << " " << publicacion.day.to_s << ", " << publicacion.year.to_s << ")\n"
			isbn.each do |i|
				if(i.length < 12)
					final << "ISBN-10: " << i
				else
					final << "ISBN-13: " << i
				end
				final << "\n" unless i == isbn.last
			end
			return final
		end
	end
end