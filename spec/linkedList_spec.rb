require "spec_helper"

describe LinkedList::LinkedList do
	before :each do
		@lista = LinkedList::LinkedList.new
		@lista_insertada = LinkedList::LinkedList.new
		@lista_insertada.insert_end(9)
		@lista_insertada.insert_end(10)
		@lista_insertada.insert_end(1)
		@lista_insertada.insert_end(12)

		#Crear los libros
		@libro1 = RefBiblio::Referencia.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"], "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers' Guide", "Pragmatic Bookshelf", 4, Date.new(2013,7,7), ["978-1937785499","1937785491"], "(The Facets of Ruby)")
		@libro2 = RefBiblio::Referencia.new(["Scott Chacon"], "Pro Git 2009th Edition", "Apress", 2009, Date.new(2009,8,27), ["978-1430218333", "1430218339"], "(Pro)")
		@libro3 = RefBiblio::Referencia.new(["David Flanagan", "Yukihiro Matsumoto"], "The Ruby Programming Language", "O’Reilly Media", 1, Date.new(2008,2,4), ["0596516177", "978-0596516178"])
		@libro4 = RefBiblio::Referencia.new(["David Chelimsky", "Dave Astels", "Bryan Helmkamp", "Dan North", "Zach Dennis", "Aslak Hellesoy"], "The RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends", "Pragmatic Bookshelf", 1, Date.new(2010,12,25), ["1934356379","978-1934356371"], "(The Facets of Ruby)")
		@libro5 = RefBiblio::Referencia.new(["Richard E. Silverman"], "Git Pocket Guide", "O’Reilly Media", 1, Date.new(2013,8,2), ["1449325866", "978-1449325862"])
	end
	describe "Existe la estructura nodo" do
		it "Debe existir el nodo" do
			expect(Struct::Node.new(1, nil)).to be_instance_of(Struct::Node)
		end
	end
	describe "Métodos para imprimir" do
		it "Imprimir una lista vacia" do
			expect(@lista.to_s).to eq("[]")
		end
		it "Imprimir una lista con varios elementos" do
			@lista.insert_end(4)
			@lista.insert_end(5)
			@lista.insert_end(3)
			@lista.insert_end(8)
			expect(@lista.to_s).to eq("[4, 5, 3, 8]")
		end
	end
	describe "Métodos para insertar" do
		it "Insertar por el final" do
			@lista.insert_end(4)
			@lista.insert_end(5)
			@lista.insert_end(3)
			@lista.insert_end(8)
			expect(@lista.to_s).to eq("[4, 5, 3, 8]")
		end
		it "Insertar por el principio" do
			@lista.insert_beg(4)
			@lista.insert_beg(5)
			@lista.insert_beg(3)
			@lista.insert_beg(8)
			expect(@lista.to_s).to eq("[8, 3, 5, 4]")
		end
	end
	describe "Métodos para extraer" do
		it "Extraer por el final" do
			@lista_insertada.extract_end
			expect(@lista_insertada.to_s).to eq("[9, 10, 1]")
		end
		it "Extraer por el inicio" do
			@lista_insertada.extract_beg
			expect(@lista_insertada.to_s).to eq("[10, 1, 12]")
		end
	end
	describe "Métodos para acceder a los datos" do
		it "Acceder con [] a los elementos" do
			expect(@lista_insertada[0].to_s).to eq("9")
			expect(@lista_insertada[2].to_s).to eq("1")
		end
		it "Verificar el tamaño de la lista" do
			expect(@lista.size.to_s).to eq("0")
			expect(@lista_insertada.size.to_s).to eq("4")
		end
	end
	describe "Métodos que usan enumeración" do
		it "Hacer una enumeración" do
			str = "["
			@lista_insertada.each{|i|
				str << i.to_s << ", "
			}
			str << "]"
			expect(str).to eq("[9, 10, 1, 12, ]")
		end
		it "Obtener el máximo de la lista" do
			expect(@lista_insertada.max.to_s).to eq("12")
		end
	end
	describe "Verificar excepciones" do
		it "Extrar de lista vacia por el final" do
			expect{@lista.extract_end}.to raise_error(RuntimeError, "List is empty, you can't extract")
		end
		it "Extrar de lista vacia por el principio" do
			expect{@lista.extract_beg}.to raise_error(RuntimeError, "List is empty, you can't extract")
		end
		it "Poner un índice erróneo" do
			expect{@lista[-1]}.to raise_error(RuntimeError, "That index doesn't exists")
		end
	end
	describe "Crear lista con referencias a libros" do
		it "Crear las referencias" do
			expect(@libro1.to_s).to eq("Dave Thomas, Andy Hunt, Chad Fowler.\nProgramming Ruby 1.9 & 2.0: The Pragmatic Programmers' Guide\n(The Facets of Ruby)\nPragmatic Bookshelf; 4 edition (July 7, 2013)\nISBN-13: 978-1937785499\nISBN-10: 1937785491")
			expect(@libro2.to_s).to eq("Scott Chacon.\nPro Git 2009th Edition\n(Pro)\nApress; 2009 edition (August 27, 2009)\nISBN-13: 978-1430218333\nISBN-10: 1430218339")
			expect(@libro3.to_s).to eq("David Flanagan, Yukihiro Matsumoto.\nThe Ruby Programming Language\nO’Reilly Media; 1 edition (February 4, 2008)\nISBN-10: 0596516177\nISBN-13: 978-0596516178")
			expect(@libro4.to_s).to eq("David Chelimsky, Dave Astels, Bryan Helmkamp, Dan North, Zach Dennis, Aslak Hellesoy.\nThe RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends\n(The Facets of Ruby)\nPragmatic Bookshelf; 1 edition (December 25, 2010)\nISBN-10: 1934356379\nISBN-13: 978-1934356371")
			expect(@libro5.to_s).to eq("Richard E. Silverman.\nGit Pocket Guide\nO’Reilly Media; 1 edition (August 2, 2013)\nISBN-10: 1449325866\nISBN-13: 978-1449325862")
		end
		it "Insertar las cosas en la lista" do
			@lista.insert_end(@libro1)
			@lista.insert_end(@libro2)
			@lista.insert_end(@libro3)
			@lista.insert_end(@libro4)
			@lista.insert_end(@libro5)
			expect(@lista.to_s).to eq("[Dave Thomas, Andy Hunt, Chad Fowler.\nProgramming Ruby 1.9 & 2.0: The Pragmatic Programmers' Guide\n(The Facets of Ruby)\nPragmatic Bookshelf; 4 edition (July 7, 2013)\nISBN-13: 978-1937785499\nISBN-10: 1937785491, Scott Chacon.\nPro Git 2009th Edition\n(Pro)\nApress; 2009 edition (August 27, 2009)\nISBN-13: 978-1430218333\nISBN-10: 1430218339, David Flanagan, Yukihiro Matsumoto.\nThe Ruby Programming Language\nO’Reilly Media; 1 edition (February 4, 2008)\nISBN-10: 0596516177\nISBN-13: 978-0596516178, David Chelimsky, Dave Astels, Bryan Helmkamp, Dan North, Zach Dennis, Aslak Hellesoy.\nThe RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends\n(The Facets of Ruby)\nPragmatic Bookshelf; 1 edition (December 25, 2010)\nISBN-10: 1934356379\nISBN-13: 978-1934356371, Richard E. Silverman.\nGit Pocket Guide\nO’Reilly Media; 1 edition (August 2, 2013)\nISBN-10: 1449325866\nISBN-13: 978-1449325862]")
		end
	end
end