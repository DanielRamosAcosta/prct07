module LinkedList
	Struct.new("Node", :value, :next)

	class LinkedList
		include Comparable
		include Enumerable
		def initialize()
			@head = nil
			@tail = nil
		end
		def to_s
			iterator = @head
			string = "["
				while !iterator.nil?
					string << iterator[:value].to_s
					string << ", " unless iterator[:next] == nil
					iterator = iterator[:next]
				end
			string << "]"
			return string
		end
		def insert_end(val)
			if @head.nil?
				@head = Struct::Node.new(val, nil)
				@tail = @head
			else
				@tail[:next] = Struct::Node.new(val, nil)
				@tail = @tail[:next]
			end
		end
		def insert_beg(val)
			if @head.nil?
				@head = Struct::Node.new(val, nil)
				@tail = @head
			else
				@head = Struct::Node.new(val, @head)
			end
		end
		def extract_end()
			if @head.nil?
				raise RuntimeError, "List is empty, you can't extract"
			end
			if @head == @tail
				@head, @tail = nil
			else
				@tail = @head
				while @tail[:next][:next] != nil
					@tail = @tail[:next]
				end
				@tail[:next] = nil
			end
		end
		def extract_beg()
			if @head.nil?
				raise RuntimeError, "List is empty, you can't extract"
			end
			if @head == @tail
				@head, @tail = nil
			else
				@head = @head[:next]
			end
		end
		def [](index)
			actual = 0
			iterator = @head
			while (!iterator.nil?) && (actual < index)
				iterator = iterator[:next]
				actual+=1
			end
			if(actual != index)
				raise RuntimeError, "That index doesn't exists"
			end
			return iterator[:value]
		end
		def size
			size = 0
			iterator = @head
			while (!iterator.nil?)
				iterator = iterator[:next]
				size+=1
			end
			return size
		end
		def each(&block)
			actual = 0
			iterator = @head
			while !iterator.nil?
				block.call(iterator[:value])
				iterator = iterator[:next]
			end
		end
	end
end